#!/usr/bin/make

SHELL := /bin/bash # Use bash syntax
DOCKER_COMPOSE_EXEC_APP_NO_TTY := docker-compose exec -T -u docker-user app
DOCKER_COMPOSE_EXEC_APP := docker-compose exec -u docker-user app

list: ## Show available commands list
	@printf "\033[33m%s:\033[0m\n" 'Available commands'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[32m%-18s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

init: ## Generate .env file, install composer dependencies, setup Laravel
	@$(MAKE) --no-print-directory env
	@$(MAKE) --no-print-directory build
	@$(MAKE) --no-print-directory clean
	@$(MAKE) --no-print-directory up

docker-install: ## install docker and docker-compose for Ubuntu
	# install docker 19.03
	curl -fsSL https://get.docker.com -o get-docker.sh
	sudo sh get-docker.sh
	sudo usermod -aG docker $(USERNAME)
	#install docker-compose 1.25
	sudo curl -L https://github.com/docker/compose/releases/download/1.25.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
	sudo chmod +x /usr/local/bin/docker-compose

env: ## Make .env file
	@ls $(shell pwd)/.env >> /dev/null 2>&1 || cp .env.example .env
	@sh .docker/scripts/set_docker_env.sh >> /dev/null 2>&1

build: ## Build docker
	@docker-compose build

up: ## Up doker
	@$(MAKE) --no-print-directory bash-history
	@docker-compose up -d --remove-orphans
	@${DOCKER_COMPOSE_EXEC_APP_NO_TTY} composer install

bash-history: ## Generate shell history file clones
	@mkdir -p $(shell pwd)/.docker/app/home/bash
	@mkdir -p $(shell pwd)/.docker/app/home/psysh
	@touch $(shell pwd)/.docker/app/home/bash/.bash_history
	@touch $(shell pwd)/.docker/app/home/psysh/psysh_history

log-rights: ## create necessary rights for logs
	sudo chmod -R 777 storage/logs

clean: ## stop all docker containers, delete all networks, volumes and stopped containers
	@[[ $$(docker ps -aq) ]] \
	&& printf "\033[33m%s:\033[0m\n" 'Stop all containers' \
	&& docker stop $$(docker ps -aq) \
	&& docker system prune -f \
	|| printf "Nothing to clean...\n"

composer-install: ## alias to composer install.
	@${DOCKER_COMPOSE_EXEC_APP} composer install

composer-require: ## alias to composer install. Should be used with variable PACKAGE. make composer-require PACKAGE=package-name
	@${DOCKER_COMPOSE_EXEC_APP} composer require ${PACKAGE}

composer-update: ## alias to composer install. Should be used with variable PACKAGE. make composer-install PACKAGE=package-name
	@${DOCKER_COMPOSE_EXEC_APP} composer update ${PACKAGE}

