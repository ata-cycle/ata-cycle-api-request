<?php

namespace Ata\Cycle\ApiRequest\Tests\Models;

use Ata\Cycle\ApiRequest\Annotations\ApiRequest;
use Ata\Cycle\ApiRequest\Annotations\RequestField;
use Ata\Cycle\ApiRequest\Annotations\StringRequestField;
use Ata\Cycle\ApiRequest\Traits\RequestQuerying;
use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\HasMany;
use Cycle\Annotated\Annotation\Relation\Inverse;

/**
 * @Entity
 * @ApiRequest(
 *     fields={
 *          @StringRequestField(name="stringField", column="string_field"),
 *          @RequestField(name="integerField", column="integer_field"),
 *     },
 *     defaultFields={
 *          "stringField",
 *          "integerField",
 *     },
 *     defaultIncludes={"related"},
 *     defaultSort="stringField",
 *     appends={"computedField", "anotherComputedField"},
 *     defaultAppends={"computedField", "anotherComputedField"},
 * )
 */
class TestModel extends BaseTestModel
{
    use RequestQuerying;

    /**
     * @HasMany(target=TestRelatedModel::class, inverse = @Inverse(as = "parent", type = "belongsTo"))
     */
    public $related;

    public function computedField(){
        return strrev($this->string_field);
    }

    public function anotherComputedField(){
        return strrev($this->string_field);
    }
}
