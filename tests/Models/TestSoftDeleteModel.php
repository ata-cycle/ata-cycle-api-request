<?php
declare(strict_types=1);

namespace Ata\Cycle\ApiRequest\Tests\Models;

use Ata\Cycle\ApiRequest\Annotations\ApiRequest;
use Ata\Cycle\ApiRequest\Traits\RequestQuerying;
use Ata\Cycle\ORM\Models\Traits\SoftDeletes;
use Ata\Cycle\ORM\Models\Traits\Timestamps;
use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\Entity;
use Ata\Cycle\ORM\Constrains\NotDeletedConstrain;

/**
 * @Entity(constrain=NotDeletedConstrain::class)
 *
 * @ApiRequest
*/
class TestSoftDeleteModel extends BaseTestModel
{
    use Timestamps;
    use SoftDeletes;
    use RequestQuerying;
}
