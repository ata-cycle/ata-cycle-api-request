<?php

namespace Ata\Cycle\ApiRequest\Tests\Models;

use Ata\Cycle\ApiRequest\Annotations\ApiRequest;
use Ata\Cycle\ApiRequest\Annotations\RequestField;
use Ata\Cycle\ApiRequest\Annotations\StringRequestField;
use Ata\Cycle\ApiRequest\Traits\RequestQuerying;
use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\HasMany;
use Cycle\Annotated\Annotation\Relation\Inverse;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @ApiRequest(
 *     fields={
 *          @StringRequestField(name="stringField", column="string_field"),
 *          @RequestField(name="integerField", column="integer_field"),
 *     },
 *     defaultFields={
 *          "stringField",
 *          "integerField",
 *     },
 *     includes={"related":TestRelatedModel::class},
 *     defaultSort="-stringField"
 * )
 */
class TestDescSortModel extends BaseTestModel
{
    use RequestQuerying;

    /**
     * @HasMany(target=TestRelatedModel::class, inverse = @Inverse(as = "parent_desc_sort", type = "belongsTo"), nullable=true)
     * @var ArrayCollection
     */
    public $related;
}
