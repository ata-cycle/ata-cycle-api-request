<?php
declare(strict_types=1);

namespace Ata\Cycle\ApiRequest\Tests\Models;

use Ata\Cycle\ApiRequest\Annotations\ApiRequest;
use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\HasMany;

/**
 * @Entity
 *
 * @ApiRequest
*/
class TestModelWithoutFieldsDefinition extends BaseTestModel
{
    /**
     * @HasMany(target=TestRelatedModel::class, nullable=true)
    */
    public $related;
}
