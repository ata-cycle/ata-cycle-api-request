<?php

namespace Ata\Cycle\ApiRequest\Tests\Models;

use Ata\Cycle\ApiRequest\Annotations\ApiRequest;
use Ata\Cycle\ApiRequest\Annotations\RequestField;
use Ata\Cycle\ApiRequest\Annotations\StringRequestField;
use Ata\Cycle\ApiRequest\Traits\RequestQuerying;
use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\BelongsTo;

/**
 * @Entity
 * @ApiRequest(
 *     fields={
 *          @StringRequestField(name="stringField", column="string_field"),
 *          @RequestField(name="integerField", column="integer_field"),
 *     },
 *     defaultFields={
 *          "stringField",
 *          "integerField",
 *     },
 *     includes={"parent":TestModel::class},
 *     defaultSort="parent.integerField"
 * )
 */
class TestRelatedSortModel extends BaseTestModel
{
    use RequestQuerying;

    /**
     * @BelongsTo(target=TestModel::class)
     * @var TestModel
     */
    public $parent;
}
