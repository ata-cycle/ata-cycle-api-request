<?php

namespace Ata\Cycle\ApiRequest\Tests\Models;

use Ata\Cycle\ApiRequest\Annotations\ApiRequest;
use Ata\Cycle\ApiRequest\Annotations\RequestField;
use Ata\Cycle\ApiRequest\Annotations\StringRequestField;
use Ata\Cycle\ApiRequest\Traits\RequestQuerying;
use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\Entity;

/**
 * @Entity
 * @ApiRequest(
 *     fields={
 *          @StringRequestField(name="string_field"),
 *          @RequestField(name="integer_field"),
 *     },
 *     defaultFields={
 *          "string_field",
 *          "integer_field",
 *     },
 *     defaultSort="integer_field",
 *     includes = {
 *          "parent": TestModel::class
 *     }
 * )
 */
class TestRelatedModel extends BaseTestModel
{
    use RequestQuerying;

    public $parent;
}
