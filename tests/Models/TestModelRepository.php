<?php

namespace Ata\Cycle\ApiRequest\Tests\Models;

use Cycle\ORM\Select\Repository;

class TestModelRepository extends Repository
{
    function relatedCount(){
        return $this->select()->with('related')->count('related.id');
    }
}
