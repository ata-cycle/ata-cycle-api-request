<?php
declare(strict_types=1);

namespace Ata\Cycle\ApiRequest\Tests\Unit;

use Ata\Cycle\ApiRequest\Schema\ApiRequestSchema;
use Ata\Cycle\ApiRequest\Tests\Models\TestModelWithoutFieldsDefinition;
use Ata\Cycle\ApiRequest\Tests\Models\TestRelatedModel;
use Ata\Cycle\ApiRequest\Tests\TestCase;

class WithoutDefinitionTest extends TestCase
{
    public function testShouldFindAllFieldsInEntity()
    {
        $annotation = resolve(ApiRequestSchema::class)->getAnnotation(TestModelWithoutFieldsDefinition::class);

        $fields = $annotation->getFields();

        self::assertTrue(
            $fields->contains(function($elem){
                return $elem->name === 'boolean_field';
            })
        );
        self::assertTrue(
            $fields->contains(function($elem){
                return $elem->name === 'integer_field';
            })
        );
        self::assertTrue(
            $fields->contains(function($elem){
                return $elem->name === 'string_field';
            })
        );
        self::assertTrue(
            $fields->contains(function($elem){
                return $elem->name === 'decimal_field';
            })
        );
        self::assertTrue(
            $fields->contains(function($elem){
                return $elem->name === 'datetime_field';
            })
        );
        self::assertTrue(
            $fields->contains(function($elem){
                return $elem->name === 'date_field';
            })
        );
        self::assertTrue(
            $fields->contains(function($elem){
                return $elem->name === 'timestamp_field';
            })
        );
        self::assertTrue(
            $fields->contains(function($elem){
                return $elem->name === 'json_field';
            })
        );
    }

    public function testShouldFindAllIncludesInEntity()
    {
        $annotation = resolve(ApiRequestSchema::class)->getAnnotation(TestModelWithoutFieldsDefinition::class);

        $includes = $annotation->getIncludes();

        self::assertEquals(1,$includes->count());
        self::assertTrue($includes->has('related'));
        self::assertEquals(TestRelatedModel::class, $includes['related']);
    }
}
