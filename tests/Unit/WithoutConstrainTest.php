<?php
declare(strict_types=1);

namespace Ata\Cycle\ApiRequest\Tests\Unit;

use Ata\Cycle\ApiRequest\Tests\Models\TestMultipleSortModel;
use Ata\Cycle\ApiRequest\Tests\Models\TestSoftDeleteModel;
use Ata\Cycle\ApiRequest\Tests\TestCase;

class WithoutConstrainTest extends TestCase
{
    protected function withTimestamps()
    {
        return true;
    }

    protected function createEntities()
    {
        TestSoftDeleteModel::create(['string_field'=>'value1']);
        TestSoftDeleteModel::create(['string_field'=>'value2']);
        TestSoftDeleteModel::create(['string_field'=>'value3']);

        $model = TestSoftDeleteModel::create(['string_field'=>'value4']);
        $model->delete();
    }

    public function testShouldReturnAllEntities()
    {
        $this->createRequest(['without_constrains' => true]);

        $result = TestSoftDeleteModel::forRequest()->findAll();

        self::assertEquals(4, $result->count());
        self::assertNotNull($result->last());
    }
}
