<?php

namespace Ata\Cycle\ApiRequest\Tests\Unit;

use Ata\Cycle\ApiRequest\Exceptions\CannotSortException;
use Ata\Cycle\ApiRequest\Tests\Models\TestCannotSortModel;
use Ata\Cycle\ApiRequest\Tests\Models\TestDescSortModel;
use Ata\Cycle\ApiRequest\Tests\Models\TestModel;
use Ata\Cycle\ApiRequest\Tests\Models\TestMultipleDirectionsSortModel;
use Ata\Cycle\ApiRequest\Tests\Models\TestMultipleSortModel;
use Ata\Cycle\ApiRequest\Tests\Models\TestRelatedModel;
use Ata\Cycle\ApiRequest\Tests\Models\TestRelatedSortModel;
use Ata\Cycle\ApiRequest\Tests\TestCase;
use Doctrine\Common\Collections\ArrayCollection;
use Jchook\AssertThrows\AssertThrows;

class SortTest extends TestCase
{
    use AssertThrows;

    public function testShouldDefaultSort()
    {
        (new TestModel(['string_field' => 'value3']))->save();
        (new TestModel(['string_field' => 'value5']))->save();
        (new TestModel(['string_field' => 'value1']))->save();
        (new TestModel(['string_field' => 'value2']))->save();

        $this->createRequest();

        $result = TestModel::forRequest()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals('value1', $result[0]->string_field);
        $this->assertEquals('value2', $result[1]->string_field);
        $this->assertEquals('value3', $result[2]->string_field);
        $this->assertEquals('value5', $result[3]->string_field);
    }

    public function testShouldDefaultSortOnMultipleEntityFields()
    {
        (new TestMultipleSortModel(['string_field' => 'value3', 'integer_field' => 2]))->save();
        (new TestMultipleSortModel(['string_field' => 'value3', 'integer_field' => 3]))->save();
        (new TestMultipleSortModel(['string_field' => 'value1', 'integer_field' => 4]))->save();
        (new TestMultipleSortModel(['string_field' => 'value2', 'integer_field' => 1]))->save();

        $this->createRequest();

        $result = TestMultipleSortModel::forRequest()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals('value1', $result[0]->string_field);
        $this->assertEquals(4, $result[0]->integer_field);
        $this->assertEquals('value2', $result[1]->string_field);
        $this->assertEquals(1, $result[1]->integer_field);
        $this->assertEquals('value3', $result[2]->string_field);
        $this->assertEquals(2, $result[2]->integer_field);
        $this->assertEquals('value3', $result[3]->string_field);
        $this->assertEquals(3, $result[3]->integer_field);
    }

    public function testShouldDefaultSortOnRelatedEntityField()
    {
        (new TestRelatedSortModel(['string_field' => 'value30', 'parent' => new TestModel(['integer_field' => 2])]))->save();
        (new TestRelatedSortModel(['string_field' => 'value20', 'parent' => new TestModel(['integer_field' => 1])]))->save();
        (new TestRelatedSortModel(['string_field' => 'value18', 'parent' => new TestModel(['integer_field' => 3])]))->save();
        (new TestRelatedSortModel(['string_field' => 'value14', 'parent' => new TestModel(['integer_field' => 4])]))->save();

        $this->createRequest();

        $result = TestRelatedSortModel::forRequest()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals('value20', $result[0]->string_field);
        $this->assertEquals('value30', $result[1]->string_field);
        $this->assertEquals('value18', $result[2]->string_field);
        $this->assertEquals('value14', $result[3]->string_field);
    }

    public function testShouldDefaultSortOnEntityAndRelatedEntityField()
    {
        (new TestModel([
            'string_field' => 'value3',
            'related' => new ArrayCollection([
                new TestRelatedModel(['integer_field' => 3]),
                new TestRelatedModel(['integer_field' => 2]),
                new TestRelatedModel(['integer_field' => 1]),
                new TestRelatedModel(['integer_field' => 4]),
            ])
        ]))->save();

        (new TestModel([
            'string_field' => 'value5',
            'related' => new ArrayCollection([
                new TestRelatedModel(['integer_field' => 3]),
                new TestRelatedModel(['integer_field' => 2]),
                new TestRelatedModel(['integer_field' => 1]),
                new TestRelatedModel(['integer_field' => 4]),
            ])
        ]))->save();

        (new TestModel([
            'string_field' => 'value1',
            'related' => new ArrayCollection([
                new TestRelatedModel(['integer_field' => 3]),
                new TestRelatedModel(['integer_field' => 2]),
                new TestRelatedModel(['integer_field' => 1]),
                new TestRelatedModel(['integer_field' => 4]),
            ])
        ]))->save();
        (new TestModel([
            'string_field' => 'value2',
            'related' => new ArrayCollection([
                new TestRelatedModel(['integer_field' => 3]),
                new TestRelatedModel(['integer_field' => 2]),
                new TestRelatedModel(['integer_field' => 1]),
                new TestRelatedModel(['integer_field' => 4]),
            ])
        ]))->save();

        resolve('cycle-db.heap-clean');
        $this->createRequest();

        $result = TestModel::forRequest()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals('value1', $result[0]->string_field);
        $this->assertEquals('value2', $result[1]->string_field);
        $this->assertEquals('value3', $result[2]->string_field);
        $this->assertEquals('value5', $result[3]->string_field);

        $result->each(function ($model) {
            $this->assertNotEmpty($model->related);
            $this->assertEquals(1, $model->related[0]->integer_field);
            $this->assertEquals(2, $model->related[1]->integer_field);
            $this->assertEquals(3, $model->related[2]->integer_field);
            $this->assertEquals(4, $model->related[3]->integer_field);
        });
    }

    public function testShouldDefaultSortWithDescDirection()
    {
        (new TestDescSortModel(['string_field' => 'value3']))->save();
        (new TestDescSortModel(['string_field' => 'value5']))->save();
        (new TestDescSortModel(['string_field' => 'value1']))->save();
        (new TestDescSortModel(['string_field' => 'value2']))->save();

        $this->createRequest();

        $result = TestDescSortModel::forRequest()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals('value5', $result[0]->string_field);
        $this->assertEquals('value3', $result[1]->string_field);
        $this->assertEquals('value2', $result[2]->string_field);
        $this->assertEquals('value1', $result[3]->string_field);
    }

    public function testShouldDefaultSortWithDifferentDirections()
    {
        (new TestMultipleDirectionsSortModel(['string_field' => 'value3', 'integer_field' => 2]))->save();
        (new TestMultipleDirectionsSortModel(['string_field' => 'value3', 'integer_field' => 3]))->save();
        (new TestMultipleDirectionsSortModel(['string_field' => 'value1', 'integer_field' => 4]))->save();
        (new TestMultipleDirectionsSortModel(['string_field' => 'value2', 'integer_field' => 1]))->save();

        $this->createRequest();

        $result = TestMultipleDirectionsSortModel::forRequest()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals('value1', $result[0]->string_field);
        $this->assertEquals(4, $result[0]->integer_field);
        $this->assertEquals('value2', $result[1]->string_field);
        $this->assertEquals(1, $result[1]->integer_field);
        $this->assertEquals('value3', $result[2]->string_field);
        $this->assertEquals(3, $result[2]->integer_field);
        $this->assertEquals('value3', $result[3]->string_field);
        $this->assertEquals(2, $result[3]->integer_field);
    }

    public function testShouldSortByRequestSortFields()
    {
        (new TestModel(['integer_field' => 3]))->save();
        (new TestModel(['integer_field' => 2]))->save();
        (new TestModel(['integer_field' => 5]))->save();
        (new TestModel(['integer_field' => 1]))->save();

        $this->createRequest([
            'sort' => 'integerField'
        ]);

        $result = TestModel::forRequest()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(1, $result[0]->integer_field);
        $this->assertEquals(2, $result[1]->integer_field);
        $this->assertEquals(3, $result[2]->integer_field);
        $this->assertEquals(5, $result[3]->integer_field);
    }

    public function testShouldSortByRequestWithMultipleDirections()
    {
        (new TestModel(['integer_field' => 3]))->save();
        (new TestModel(['integer_field' => 2]))->save();
        (new TestModel(['integer_field' => 5]))->save();
        (new TestModel(['integer_field' => 1]))->save();

        $this->createRequest([
            'sort' => '-integerField'
        ]);

        $result = TestModel::forRequest()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(5, $result[0]->integer_field);
        $this->assertEquals(3, $result[1]->integer_field);
        $this->assertEquals(2, $result[2]->integer_field);
        $this->assertEquals(1, $result[3]->integer_field);
    }

    public function testShouldSortRelatedEntityByRequest()
    {
        (new TestModel([
            'integer_field' => 3,
            'related' => new ArrayCollection([
                new TestRelatedModel(['string_field' => 'value3']),
                new TestRelatedModel(['string_field' => 'value2']),
                new TestRelatedModel(['string_field' => 'value5']),
                new TestRelatedModel(['string_field' => 'value1']),
            ])
        ]))->save();

        $this->createRequest([
            'sort' => [
                'related' => 'string_field'
            ]
        ]);

        $result = TestModel::forRequest()->findAll();

        $this->assertNotEmpty($result);

        $result->each(function ($model) {
            $this->assertNotEmpty($model->related);
            $this->assertEquals('value1', $model->related[0]->string_field);
            $this->assertEquals('value2', $model->related[1]->string_field);
            $this->assertEquals('value3', $model->related[2]->string_field);
            $this->assertEquals('value5', $model->related[3]->string_field);
        });
    }

    public function testShouldThrowExceptionIfCannotSortOnField()
    {
        (new TestCannotSortModel(['string_field' => 'value3']))->save();
        (new TestCannotSortModel(['string_field' => 'value5']))->save();
        (new TestCannotSortModel(['string_field' => 'value1']))->save();
        (new TestCannotSortModel(['string_field' => 'value2']))->save();

        $this->createRequest([
            'sort' => 'string_field'
        ]);

        $this->assertThrows(CannotSortException::class, function () {
            TestCannotSortModel::forRequest()->findAll();
        });
    }

    public function testShouldThrowExceptionIfCannotSortOnFieldWhenTryToSortOnMultipleFields()
    {
        (new TestCannotSortModel(['string_field' => 'value3']))->save();
        (new TestCannotSortModel(['string_field' => 'value5']))->save();
        (new TestCannotSortModel(['string_field' => 'value1']))->save();
        (new TestCannotSortModel(['string_field' => 'value2']))->save();

        $this->createRequest([
            'sort' => '-integer_field,string_field'
        ]);

        $this->assertThrows(CannotSortException::class, function ()  {
            TestCannotSortModel::forRequest()->findAll();
        });
    }
}
