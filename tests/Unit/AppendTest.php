<?php


namespace Ata\Cycle\ApiRequest\Tests\Unit;


use Ata\Cycle\ApiRequest\Exceptions\CannotFindAppendException;
use Ata\Cycle\ApiRequest\Tests\Models\TestModel;
use Ata\Cycle\ApiRequest\Tests\TestCase;
use ErrorException;
use Illuminate\Http\Request;
use Jchook\AssertThrows\AssertThrows;

class AppendTest extends TestCase
{
    use AssertThrows;

    public function testShouldAppendFieldsByDefault(){
        (new TestModel(['string_field'=>'value']))->save();
        $this->createRequest([]);
        $result = TestModel::forRequest()->findAll();

        $this->assertEquals(strrev('value'), $result[0]->computedField);
        $this->assertEquals(strrev('value'), $result[0]->anotherComputedField);
    }


    public function testShouldAppendFieldsFromRequest()
    {
        (new TestModel(['string_field'=>'value']))->save();

        $this->createRequest(
            ['append' => 'computedField']
        );

        $result = TestModel::forRequest()->findAll();

        $this->assertEquals(strrev('value'), $result[0]->computedField);

        $this->assertThrows(ErrorException::class, function () use($result){
            $result[0]->anotherComputedField;
        });
    }

    public function testShouldThrowExceptionIfAppendFieldNotIncludedInAnnotation()
    {
        (new TestModel(['string_field'=>'value']))->save();

        $this->createRequest(
            ['append' => 'differentField']
        );

        $this->assertThrows(CannotFindAppendException::class, function () {
            TestModel::forRequest()->findAll();
        });
    }
}
