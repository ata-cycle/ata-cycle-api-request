<?php


namespace Ata\Cycle\ApiRequest\Tests\Unit;


use Ata\Cycle\ApiRequest\Tests\Models\TestModel;
use Ata\Cycle\ApiRequest\Tests\Models\TestRelatedModel;
use Ata\Cycle\ApiRequest\Tests\TestCase;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PaginationTest extends TestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    public function testShouldPaginateWithDefaultPageCount()
    {
        $transaction = resolve('cycle-db.transaction');

        Collection::times(50, function ($number) use (
            $transaction
        ) {
            $transaction->persist((new TestModel(['integer_field' => $number])));
        });

        $transaction->run();

        $this->createRequest([
            'page' => 3,
            'sort' => 'integerField,related.integer_field'
        ]);

        $result = TestModel::withPagination()->withSorts()->findAll();

        $this->assertEquals(10, $result->count());
        $this->assertEquals(21, $result[0]->integer_field);
        $this->assertEquals(22, $result[1]->integer_field);
        $this->assertEquals(23, $result[2]->integer_field);
        $this->assertEquals(24, $result[3]->integer_field);
        $this->assertEquals(25, $result[4]->integer_field);
        $this->assertEquals(26, $result[5]->integer_field);
        $this->assertEquals(27, $result[6]->integer_field);
        $this->assertEquals(28, $result[7]->integer_field);
        $this->assertEquals(29, $result[8]->integer_field);
        $this->assertEquals(30, $result[9]->integer_field);
    }

    public function testShouldPaginateWithIncomingPageCount()
    {
        $transaction = resolve('cycle-db.transaction');

        Collection::times(50, function ($number) use ($transaction) {
            $transaction->persist((new TestModel(['integer_field' => $number])));
        });

        $transaction->run();

        $this->createRequest([
            'page' => 3,
            'per_page' => 2,
            'sort' => 'integerField'
        ]);

        $result = TestModel::withPagination()->withSorts()->findAll();

        $this->assertEquals(2, $result->count());

        $this->assertEquals(5, $result[0]->integer_field);
        $this->assertEquals(6, $result[1]->integer_field);
    }

    public function testShouldPaginateAndShowRelatedEntities()
    {
        $transaction = resolve('cycle-db.transaction');

        Collection::times(50, function ($number) use ($transaction) {
            $transaction->persist((new TestModel(['integer_field' => $number, 'related' => new ArrayCollection([
                        new TestRelatedModel(['string_field' => 'asdf' . $number]),
                        new TestRelatedModel(['string_field' => 'asdf' . ($number+1)]),
                    ]
                )]
            )
            ));
        });

        $transaction->run();

        $this->createRequest([
            'page' => 3,
            'per_page' => 2,
            'sort' => 'integerField,related.string_field'
        ]);

        $result = TestModel::forRequest()->findAll();

        $this->assertEquals(2, $result->count());

        $this->assertEquals(5, $result[0]->integer_field);
        $this->assertEquals(6, $result[1]->integer_field);

        $this->assertEquals(2, $result[0]->related->count());
        $this->assertEquals('asdf5', $result[0]->related[0]->string_field);
        $this->assertEquals('asdf6', $result[0]->related[1]->string_field);

        $this->assertEquals('asdf6', $result[1]->related[0]->string_field);
        $this->assertEquals('asdf7', $result[1]->related[1]->string_field);
    }
}
