<?php


namespace Ata\Cycle\ApiRequest\Tests\Unit;

use Ata\Cycle\ApiRequest\Tests\Models\TestModel;
use Ata\Cycle\ApiRequest\Tests\Models\TestRelatedModel;
use Ata\Cycle\ApiRequest\Tests\TestCase;
use Doctrine\Common\Collections\ArrayCollection;

class FilterTest extends TestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    public function testShouldFilterViaEntityField()
    {
        TestModel::create(['string_field' => 'value']);
        TestModel::create(['string_field' => '123']);

        // /users?filter[field]=value

        $this->createRequest([
            'filter' => [
                'string_field' => 'value'
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);
        $this->assertEquals(1, $result->count());
        $this->assertEquals('value', $result[0]->string_field);
    }

    public function testShouldFilterViaRelatedEntityField()
    {
        TestModel::create(['string_field' => 'value', 'related' => [
            ['integer_field' => 1]
        ]]);
        TestModel::create(['string_field' => 'value', 'related' => [
            ['integer_field' => 2]
        ]]);

        // /users?filter[related.field]=value
        $this->createRequest([
            'filter' => [
                'related.integer_field' => 2
            ]
        ]);

        $result = TestModel::forRequest()->findAll();

        $this->assertNotEmpty($result);
        $this->assertEquals(1, $result->count());
        $this->assertEquals(2, $result->last()->related[0]->integer_field);
    }

    public function testShouldFilterRelatedEntity()
    {
        $testModel = new TestModel(['string_field' => 'value']);
        $testModel->related = new ArrayCollection([
                new TestRelatedModel(['integer_field' => 2]),
                new TestRelatedModel(['integer_field' => 1]),
                new TestRelatedModel(['integer_field' => 3]),
            ]
        );

        $testModel->save();

        // /users?filter[related][field]=value
        $this->createRequest([
            'filter' => [
                'related' => [
                    'integer_field' => 2
                ]
            ]
        ]);

        $result = TestModel::withFilters()->withIncludes()->findAll();

        $this->assertNotEmpty($result);
        $this->assertEquals(1, $result[0]->related->count());
        $this->assertEquals(2, $result[0]->related[0]->integer_field);
    }

    public function testShouldFilterEqualsOnDefaultForNonStringFields()
    {
        (new TestModel(['integer_field' => 3]))->save();
        (new TestModel(['integer_field' => 2]))->save();

        // /users?filter[related][field]=value
        $this->createRequest([
            'filter' => [
                'integer_field' => 3
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);
        $this->assertEquals(1, $result->count());
        $this->assertEquals(3, $result[0]->integer_field);
    }

    public function testShouldFilterLikeCaseInsensitiveOnDefaultForStringFields()
    {
        (new TestModel(['string_field' => 'vaLue']))->save();
        (new TestModel(['string_field' => 'vAlue']))->save();
        (new TestModel(['string_field' => 'other']))->save();

        // /users?filter[field]=value
        $this->createRequest([
            'filter' => [
                'string_field' => 'value'
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);
        $this->assertEquals(2, $result->count());
        $this->assertEquals('vAlue', $result[1]->string_field);
        $this->assertEquals('vaLue', $result[0]->string_field);
    }

    public function testShouldFilterEquals()
    {
        $testModel = new TestModel(['integer_field' => 3]);
        $testModel->save();

        // /users?filter[field:eq]=value
        $this->createRequest([
            'filter' => [
                'integerField:' . config('api_request.operators.equals.api')[0] => 3
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);
        $this->assertEquals(3, $result[0]->integer_field);
    }

    public function testShouldFilterNotEquals()
    {
        // /users?filter[field:neq]=value
        $testModel = new TestModel(['integer_field' => 3]);
        $testModel->save();

        $this->createRequest([
            'filter' => [
                'integerField:' . config('api_request.operators.not_equals.api')[0] => 3
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertEmpty($result);

        $this->createRequest([
            'filter' => [
                'integerField:' . config('api_request.operators.not_equals.api')[0] => 2
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(3, $result[0]->integer_field);
    }

    public function testShouldFilterGreaterThan()
    {
        // /users?filter[field:gt]=value
        $testModel = new TestModel(['integer_field' => 3]);
        $testModel->save();

        $this->createRequest([
            'filter' => [
                'integerField:' . config('api_request.operators.greater_than.api')[0] => 3
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertEmpty($result);

        $this->createRequest([
            'filter' => [
                'integerField:' . config('api_request.operators.greater_than.api')[0] => 2
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(3, $result[0]->integer_field);
    }

    public function testShouldFilterGreaterThanEquals()
    {
        // /users?filter[field:gte]=value
        (new TestModel(['integer_field' => 3]))->save();

        $this->createRequest([
            'filter' => [
                'integerField:' . config('api_request.operators.greater_than_equals.api')[0] => 3
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(3, $result[0]->integer_field);
    }

    public function testShouldFilterLesserThan()
    {
        // /users?filter[field:lt]=value
        (new TestModel(['integer_field' => 3]))->save();

        $this->createRequest([
            'filter' => [
                'integerField:' . config('api_request.operators.lesser_than.api')[0] => 3
            ]
        ]);

        $this->assertEmpty(TestModel::withFilters()->findAll());

        $this->createRequest([
            'filter' => [
                'integerField:' . config('api_request.operators.lesser_than.api')[0] => 4
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(3, $result[0]->integer_field);
    }

    public function testShouldFilterLesserThanEquals()
    {
        // /users?filter[field:lte]=value
        (new TestModel(['integer_field' => 3]))->save();

        $this->createRequest([
            'filter' => [
                'integerField:' . config('api_request.operators.lesser_than_equals.api')[0] => 3
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(3, $result[0]->integer_field);
    }

    public function testShouldFilterLikeCaseSensitive()
    {
        // /users?filter[field:like]=value
        (new TestModel(['string_field' => 'VALUE']))->save();
        (new TestModel(['string_field' => 'value1']))->save();

        $this->createRequest([
            'filter' => [
                'stringField:' . config('api_request.operators.like_sensitive.api')[0] => 'value'
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(1, $result->count());
        $this->assertEquals('value1', $result[0]->string_field);
    }

    public function testShouldFilterLikeCaseInsensitive()
    {
        // /users?filter[field:ilike]=value
        (new TestModel(['string_field' => 'VALUE']))->save();
        (new TestModel(['string_field' => 'value']))->save();

        $this->createRequest([
            'filter' => [
                'stringField:' . config('api_request.operators.like_insensitive.api')[0] => 'value'
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(2, $result->count());
        $this->assertEquals('VALUE', $result[0]->string_field);
        $this->assertEquals('value', $result[1]->string_field);
    }

    public function testShouldFilterInOnDefaultWhenPassMultipleValues()
    {
        // /users?filter[field]=value1,value2
        (new TestModel(['string_field' => 'VALUE']))->save();
        (new TestModel(['string_field' => 'value']))->save();

        $this->createRequest([
            'filter' => [
                'string_field' => 'value,VALUE'
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(2, $result->count());
        $this->assertEquals('VALUE', $result[0]->string_field);
        $this->assertEquals('value', $result[1]->string_field);
    }

    public function testShouldFilterIn()
    {
        // /users?filter[field:in]=value,value2
        (new TestModel(['string_field' => 'VALUE']))->save();
        (new TestModel(['string_field' => 'value']))->save();

        $this->createRequest([
            'filter' => [
                'stringField:' . config('api_request.operators.in.api')[0] => 'value,VALUE'
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(2, $result->count());
        $this->assertEquals('VALUE', $result[0]->string_field);
        $this->assertEquals('value', $result[1]->string_field);
    }

    public function testShouldFilterNotIn()
    {
        // /users?filter[field:not-in]=value1,value2
        (new TestModel(['string_field' => 'VALUE']))->save();
        (new TestModel(['string_field' => 'value']))->save();
        (new TestModel(['string_field' => 'other']))->save();

        $this->createRequest([
            'filter' => [
                'stringField:' . config('api_request.operators.not_in.api')[0] => 'value,VALUE'
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(1, $result->count());
        $this->assertEquals('other', $result[0]->string_field);
    }

    public function testShouldFilterBetween()
    {
        // /users?filter[field:beetween]=value1,value2
        (new TestModel(['string_field' => 'value0']))->save();
        (new TestModel(['string_field' => 'value1']))->save();
        (new TestModel(['string_field' => 'value2']))->save();
        (new TestModel(['string_field' => 'value3']))->save();
        (new TestModel(['string_field' => 'value4']))->save();

        $this->createRequest([
            'filter' => [
                'stringField:' . config('api_request.operators.between.api')[0] => 'value1,value3'
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(3, $result->count());
        $this->assertEquals('value1', $result[0]->string_field);
        $this->assertEquals('value2', $result[1]->string_field);
        $this->assertEquals('value3', $result[2]->string_field);
    }

    public function testShouldFilterNotBetween()
    {
        // /users?filter[field:not-beetween]=value1,value2
        (new TestModel(['string_field' => 'value0']))->save();
        (new TestModel(['string_field' => 'value1']))->save();
        (new TestModel(['string_field' => 'value2']))->save();
        (new TestModel(['string_field' => 'value3']))->save();
        (new TestModel(['string_field' => 'value4']))->save();

        $this->createRequest([
            'filter' => [
                'stringField:' . config('api_request.operators.not_between.api')[0] => 'value1,value3'
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(2, $result->count());
        $this->assertEquals('value0', $result[0]->string_field);
        $this->assertEquals('value4', $result[1]->string_field);
    }

    public function testShouldFilterIsNullWithTrue(){
        // /users?filter[field:is-null]
        (new TestModel(['string_field' => 'value0']))->save();
        (new TestModel(['string_field' => 'value1', 'integer_field' => 1]))->save();

        $this->createRequest([
            'filter' => [
                'integerField:' . config('api_request.operators.is_null.api')[0] => true
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertEquals(1, $result->count());
        $this->assertEquals('value0', $result[0]->string_field);
    }

    public function testShouldFilterIsNullWithFalse(){
        // /users?filter[field:is-null]
        (new TestModel(['string_field' => 'value0']))->save();
        (new TestModel(['string_field' => 'value1', 'integer_field' => 1]))->save();

        $this->createRequest([
            'filter' => [
                'integerField:' . config('api_request.operators.is_null.api')[0] => false
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertEquals(1, $result->count());
        $this->assertEquals('value1', $result[0]->string_field);
    }

    public function testShouldFilterIsNotNullWithTrue(){
        // /users?filter[field:is-null]
        (new TestModel(['string_field' => 'value0']))->save();
        (new TestModel(['string_field' => 'value1', 'integer_field' => 1]))->save();

        $this->createRequest([
            'filter' => [
                'integerField:' . config('api_request.operators.is_not_null.api')[0] => true
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertEquals(1, $result->count());
        $this->assertEquals('value1', $result[0]->string_field);
    }

    public function testShouldFilterIsNotNullWithFalse(){
        // /users?filter[field:is-null]
        (new TestModel(['string_field' => 'value0']))->save();
        (new TestModel(['string_field' => 'value1', 'integer_field' => 1]))->save();

        $this->createRequest([
            'filter' => [
                'integerField:' . config('api_request.operators.is_not_null.api')[0] => false
            ]
        ]);

        $result = TestModel::withFilters()->findAll();

        $this->assertEquals(1, $result->count());
        $this->assertEquals('value0', $result[0]->string_field);
    }

    public function testRelatedInverse()
    {
        $testModel = new TestModel(['string_field' => 'value']);
        $testModel->related = new ArrayCollection([
                new TestRelatedModel(['integer_field' => 2]),
                new TestRelatedModel(['integer_field' => 1]),
                new TestRelatedModel(['integer_field' => 3]),
            ]
        );

        $testModel->save();

        resolve('cycle-db.heap-clean');

        $related = TestRelatedModel::loadLeftJoin('parent')->findOne();

        $this->assertNotNull($related->parent);
        $this->assertEquals('value', $related->parent->string_field);
    }
}
