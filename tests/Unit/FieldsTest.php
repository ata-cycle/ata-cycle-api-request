<?php

namespace Ata\Cycle\ApiRequest\Tests\Unit;

use Ata\Cycle\ApiRequest\Tests\Models\TestModel;
use Ata\Cycle\ApiRequest\Tests\Models\TestRelatedModel;
use Ata\Cycle\ApiRequest\Tests\TestCase;
use Doctrine\Common\Collections\ArrayCollection;
use Jchook\AssertThrows\AssertThrows;

class FieldsTest extends TestCase
{
    use AssertThrows;


    public function testShouldIncludeDefaultFieldsIfFieldsParameterNotExistInRequest()
    {
        (new TestModel(['string_field' => 'value', 'integer_field' => 3]))->save();

        $this->createRequest([]);

        $result = TestModel::forRequest()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals('value', $result[0]->string_field);
        $this->assertEquals(3, $result[0]->integer_field);

        $this->assertNull($result[0]->boolean_field);
        $this->assertNull($result[0]->decimal_field);
        $this->assertNull($result[0]->datetime_field);
        $this->assertNull($result[0]->json_field);
    }


    public function testShouldIncludeFieldsOnlyInRequest()
    {
        // fields=field1,field2
        (new TestModel(['string_field' => 'value', 'integer_field' => 3]))->save();

        $this->createRequest([
            'fields' => [
                'stringField'
            ]
        ]);

        $result = TestModel::forRequest()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals('value', $result[0]->string_field);
        $this->assertNull($result[0]->integer_field);
        $this->assertNull($result[0]->boolean_field);
        $this->assertNull($result[0]->decimal_field);
        $this->assertNull($result[0]->datetime_field);
        $this->assertNull($result[0]->json_field);
    }

    public function testShouldIncludeDefaultRelatedEntityFieldsIfFieldsParameterNotExists()
    {
        (new TestModel(['string_field' => 'value', 'integer_field' => 3,
            'related' => new ArrayCollection([
                new TestRelatedModel(['string_field' => 'VALUE1', 'integer_field' => 1]),
                new TestRelatedModel(['string_field' => 'VALUE2', 'integer_field' => 2]),
                new TestRelatedModel(['string_field' => 'VALUE3', 'integer_field' => 3]),
            ])
        ]))->save();

        $this->createRequest(
            ['sort' => ['related' => 'string_field']]
        );

        $result = TestModel::forRequest()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(1, $result->count());

        $this->assertNotEmpty($result[0]->related);
        $this->assertEquals(3, $result[0]->related->count());

        $this->assertEquals('VALUE1', $result[0]->related[0]->string_field);
        $this->assertEquals(1, $result[0]->related[0]->integer_field);

        $this->assertEquals('VALUE2', $result[0]->related[1]->string_field);
        $this->assertEquals(2, $result[0]->related[1]->integer_field);

        $this->assertEquals('VALUE3', $result[0]->related[2]->string_field);
        $this->assertEquals(3, $result[0]->related[2]->integer_field);

        collect($result[0]->related)->each(function ($elem) {
            $this->assertNull($elem->boolean_field);
            $this->assertNull($elem->decimal_field);
            $this->assertNull($elem->datetime_field);
            $this->assertNull($elem->json_field);
        });
    }

    public function testShouldIncludeRelatedFieldsThatInRequests()
    {
        // fields=field1,field2&fields[related]=field1,field2
        (new TestModel(['string_field' => 'value', 'integer_field' => 3,
            'related' => new ArrayCollection([
                new TestRelatedModel(['string_field' => 'VALUE1', 'integer_field' => 1]),
                new TestRelatedModel(['string_field' => 'VALUE2', 'integer_field' => 2]),
                new TestRelatedModel(['string_field' => 'VALUE3', 'integer_field' => 3]),
            ])
        ]))->save();

        resolve('cycle-db.heap-clean');

        $this->createRequest([
            'fields' => [
                'related' => 'string_field'
            ],

            ['sort' => ['related' => 'string_field']]
        ]);

        $result = TestModel::forRequest()->findAll();

        $this->assertNotEmpty($result);

        $this->assertEquals(1, $result->count());

        $this->assertNotEmpty($result[0]->related);
        $this->assertEquals(3, $result[0]->related->count());

        $this->assertEquals('VALUE1', $result[0]->related[0]->string_field);

        $this->assertEquals('VALUE2', $result[0]->related[1]->string_field);

        $this->assertEquals('VALUE3', $result[0]->related[2]->string_field);

        collect($result[0]->related)->each(function ($elem) {
            $this->assertNull($elem->integer_field);
            $this->assertNull($elem->boolean_field);
            $this->assertNull($elem->decimal_field);
            $this->assertNull($elem->datetime_field);
            $this->assertNull($elem->json_field);
        });
    }
}
