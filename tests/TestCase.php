<?php

namespace Ata\Cycle\ApiRequest\Tests;

use Ata\Cycle\ApiRequest\Repositories\ApiRequestRepository;
use Ata\Cycle\ApiRequest\Tests\Models\TestModel;
use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Illuminate\Http\Request;

class TestCase extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    protected function createRequest($params = [])
    {
        $this->app->bind('request', function ($app) use ($params) {
            return new Request($params);
        });
    }

    protected function getEnvironmentSetUp($app)
    {
        $requestConfig = include __DIR__ . '/../config/api_request.php';
        $app['config']->set('api_request', $requestConfig);

        parent::getEnvironmentSetUp($app);

        $app['config']->set('cycle.schema.repository', ApiRequestRepository::class);
    }

    protected function getPackageProviders($app)
    {
        return array_merge(
           parent::getPackageProviders($app),
            [
                'Ata\Cycle\ApiRequest\PackageServiceProvider'
            ]
        );
    }
}
