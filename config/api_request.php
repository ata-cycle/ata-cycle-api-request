<?php
return [
    'operators' => [
        'equals' => [
            'api' => ['eq'],
            'db' => '=',
        ],
        'not_equals' => [
            'api' => ['neq'],
            'db' => '!=',

        ],
        'greater_than' => [
            'api' => ['gt'],
            'db' => '>',

        ],
        'greater_than_equals' => [
            'api' => ['gte'],
            'db' => '>=',

        ],
        'lesser_than' => [
            'api' => ['lt'],
            'db' => '<',

        ],
        'lesser_than_equals' => [
            'api' => ['lte'],
            'db' => '<=',

        ],
        'like_sensitive' => [
            'api' => ['like'],
            'db' => 'like',

        ],
        'like_insensitive' => [
            'api' => ['ilike'],
            'db' => 'ilike',

        ],
        'in' => [
            'api' => ['in'],
            'db' => 'in',

        ],
        'not_in' => [
            'api' => ['not-in'],
            'db' => 'not in',

        ],
        'between' => [
            'api' => ['between'],
            'db' => 'between',

        ],
        'not_between' => [
            'api' => ['not-beetween'],
            'db' => 'not between',
        ],
        'is_null' => [
            'api' => ['null'],
            'db' => 'is',
        ],
        'is_not_null' => [
            'api' => ['not-null'],
            'db' => 'is not',
        ],
    ],
    'parameters' => [
        'filter' => 'filter',
        'include' => 'include',
        'sort' => 'sort',
        'fields' => 'fields',
        'append' => 'append',
        'page' => 'page',
        'per_page' => 'per_page',
        'without_constrains' => 'without_constrains',
    ],
    'operator_delimiter' => ':',
    'related_delimiter' => '.',
    'list_delimiter' => ',',
    'default_list_operator' => 'in',
    'default_filters' => [
        \Ata\Cycle\ApiRequest\Annotations\RequestField::class => 'equals',
        \Ata\Cycle\ApiRequest\Annotations\StringRequestField::class => 'like_insensitive',
        \Ata\Cycle\ApiRequest\Annotations\DateRequestField::class => 'between',
    ],
    'default_sort_field' => 'created_at',
    'fields_sorting_by_default' => true,
    'pagination' =>  [
        'per_page' => 10,
        'enabled_by_default' => false,
    ],
];
