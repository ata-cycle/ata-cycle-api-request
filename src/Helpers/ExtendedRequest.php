<?php


namespace Ata\Cycle\ApiRequest\Helpers;


use Ata\Cycle\ApiRequest\Exceptions\CannotFindQueryParameterException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;


class ExtendedRequest extends Request
{
    public static function fromRequest(Request $request=null): self
    {
        return static::createFrom($request ?? request(), new self());
    }

    public function includes(): Collection
    {
        $includeParameterName = config('api_request.parameters.include');
        throw_if($includeParameterName === null, CannotFindQueryParameterException::class);

        $includeParts = $this->query($includeParameterName);
        if (!is_array($includeParts)) {
            $includeParts = explode(',', $this->query($includeParameterName));
        }
        return collect($includeParts)
            ->filter()
            ->map([Str::class, 'camel']);
    }

    public function appends(): Collection
    {
        $appendParameterName = config('api_request.parameters.append');
        throw_if($appendParameterName === null, CannotFindQueryParameterException::class);

        $appendParts = $this->query($appendParameterName);
        if (!is_array($appendParts)) {
            $appendParts = explode(',', $appendParts);
        }
        return collect($appendParts)->filter();
    }

    public function filters(): Collection
    {
        $filterParameterName = config('api_request.parameters.filter');
        throw_if($filterParameterName === null, CannotFindQueryParameterException::class);

        $filterParts = $this->query($filterParameterName, []);
        if (is_string($filterParts)) {
            return collect();
        }
        $filters = collect($filterParts);
        return $filters->map(function ($value, $key) {
            return new RequestFilter($key, $value);
        });
    }

    public function fields(): Collection
    {
        $fieldsParameterName = config('api_request.parameters.fields');
        throw_if($fieldsParameterName === null, CannotFindQueryParameterException::class);
        $fieldsPerTable = collect($this->query($fieldsParameterName));
        if ($fieldsPerTable->isEmpty()) {
            return collect();
        }
        return $fieldsPerTable->map(function ($fields) {
            return collect(explode(',', $fields));
        });
    }

    public function sorts(): Collection
    {
        $sortParameterName = config('api_request.parameters.sort');
        throw_if($sortParameterName === null, CannotFindQueryParameterException::class);
        $sortParts = $this->query($sortParameterName);
        if (is_string($sortParts)) {
            $sortParts = explode(',', $sortParts);
        }
        return collect($sortParts)->filter();
    }

    public function getPageInfo(): array
    {
        $pageParameterName = config('api_request.parameters.page');
        $perPageParameterName = config('api_request.parameters.per_page');

        throw_if($pageParameterName === null, CannotFindQueryParameterException::class);
        throw_if($perPageParameterName === null, CannotFindQueryParameterException::class);
        return [
            $this->query($pageParameterName),
            $this->query($perPageParameterName),
        ];
    }

    public function withoutConstrains(): bool
    {
        $withoutConstrainsParameterName = config('api_request.parameters.without_constrains');

        throw_if($withoutConstrainsParameterName === null, CannotFindQueryParameterException::class);

        $parameterValue = $this->query($withoutConstrainsParameterName);

        if ($parameterValue === null){
            return false;
        }

        return $parameterValue;
    }

    /**
     * @param $value
     *
     * @return array|bool
     */
    protected function getFilterValue($value)
    {
        if (is_array($value)) {
            return collect($value)->map(function ($valueValue) {
                return $this->getFilterValue($valueValue);
            })->all();
        }
        if (Str::contains($value, ',')) {
            return explode(',', $value);
        }
        if ($value === 'true') {
            return true;
        }
        if ($value === 'false') {
            return false;
        }
        return $value;
    }
}
