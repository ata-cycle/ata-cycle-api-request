<?php

namespace Ata\Cycle\ApiRequest\Generators;

use Cycle\Schema\GeneratorInterface;
use Cycle\Schema\Registry;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class FilterColumnsGenerator implements GeneratorInterface
{
    /**
     * @var array<string,Collection>
     */
    private $filterMap = [];

    public function addFilter($class, $columns)
    {
        $this->filterMap[$class] = $columns;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function run(Registry $registry): Registry
    {
        foreach ($registry as $entity) {
            if (!array_key_exists($entity->getClass(), $this->filterMap)) {
                continue;
            }

            foreach ($entity->getFields() as $field) {
                if ($field->isPrimary()){
                    continue;
                }

                if (Str::contains($field->getColumn(), '_id')){
                    continue;
                }

                if (!$this->filterMap[$entity->getClass()]->contains($field->getColumn())) {
                    $entity->getFields()->remove($field->getColumn());
                }
            }
        }
        return $registry;
    }
}
