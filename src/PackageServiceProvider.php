<?php


namespace Ata\Cycle\ApiRequest;


use Ata\Cycle\ApiRequest\Schema\ApiRequestSchema;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Illuminate\Support\ServiceProvider;

class PackageServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(ApiRequestSchema::class, function ($app) {

            // Бежим по сущностям, определенным в конфиге и создаем конфигурацию возможных реквестов
            $modelsPath = config('cycle.schema.path');

            $finder = (new \Symfony\Component\Finder\Finder())->files()->in($modelsPath);
            $classLocator = new \Spiral\Tokenizer\ClassLocator($finder);
            // autoload annotations
            AnnotationRegistry::registerLoader('class_exists');

            return new ApiRequestSchema($classLocator);
        });
    }

    public function boot(){

        $this->publishes([
            __DIR__ . '/../config/api_request.php' => config_path('api_request.php')
        ]);

        $this->mergeConfigFrom(
            __DIR__ . '/../config/api_request.php', 'api_request'
        );
    }
}

