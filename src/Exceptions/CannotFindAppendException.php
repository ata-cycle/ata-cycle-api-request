<?php


namespace Ata\Cycle\ApiRequest\Exceptions;


use Exception;

class CannotFindAppendException extends Exception
{
    private $fields;

    public function __construct($fields)
    {
        $this->fields = $fields;

        parent::__construct($fields);
    }
}
