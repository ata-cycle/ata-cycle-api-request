<?php


namespace Ata\Cycle\ApiRequest\Exceptions;

use Exception;

class CannotFindQueryParameterException extends Exception
{
    private $parameterName;

    public function __construct($parameterName)
    {
        $this->parameterName = $parameterName;

        parent::__construct($parameterName);
    }
}
