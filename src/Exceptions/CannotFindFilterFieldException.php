<?php


namespace Ata\Cycle\ApiRequest\Exceptions;

use Exception;

class CannotFindFilterFieldException extends Exception
{
    private $fieldName;

    public function __construct($fieldName)
    {
        $this->fieldName = $fieldName;

        parent::__construct($fieldName);
    }
}
