<?php

namespace Ata\Cycle\ApiRequest\Repositories\Interfaces;

use Ata\Cycle\ORM\Repositories\AtaRepositoryInterface;

interface ApiRequestRepositoryInterface extends AtaRepositoryInterface
{
    /**
     * Process fields parameter in request
     * @return ApiRequestRepositoryInterface
     */
    function withFields(): ApiRequestRepositoryInterface;

    /**
     * Process includes parameter in request
     * @return ApiRequestRepositoryInterface
     */
    function withIncludes(): ApiRequestRepositoryInterface;

    /**
     * Process pagination parameters in request
     * @return ApiRequestRepositoryInterface
     */
    function withPagination(): ApiRequestRepositoryInterface;

    /**
     * Process sort parameters in request
     * @return ApiRequestRepositoryInterface
     */
    function withSorts(): ApiRequestRepositoryInterface;

    /**
     * Process filters parameters in request
     * @return ApiRequestRepositoryInterface
     */
    function withFilters(): ApiRequestRepositoryInterface;

    /**
     * Process appends parameters in requests
     * @return ApiRequestRepositoryInterface
     */
    function withAppends(): ApiRequestRepositoryInterface;

    /**
     * Process request with includes, filters, pagination, sort and appends
     * @return ApiRequestRepositoryInterface
     */
    function forRequest(): ApiRequestRepositoryInterface;
}
