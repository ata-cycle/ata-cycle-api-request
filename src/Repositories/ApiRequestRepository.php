<?php

namespace Ata\Cycle\ApiRequest\Repositories;

use Ata\Cycle\ApiRequest\Annotations\ApiRequest;
use Ata\Cycle\ApiRequest\Exceptions\CannotFindAppendException;
use Ata\Cycle\ApiRequest\Exceptions\CannotQueryThroughFilterException;
use Ata\Cycle\ApiRequest\Exceptions\DenyPaginationException;
use Ata\Cycle\ApiRequest\Generators\FilterColumnsGenerator;
use Ata\Cycle\ApiRequest\Helpers\ExtendedRequest;
use Ata\Cycle\ApiRequest\Helpers\RequestFilter;
use Ata\Cycle\ApiRequest\Repositories\Interfaces\ApiRequestRepositoryInterface;
use Ata\Cycle\ApiRequest\Schema\ApiRequestSchema;
use Ata\Cycle\ORM\Helpers\SchemaCreator;
use Ata\Cycle\ORM\Repositories\AtaRepository;
use Cycle\ORM\Heap\Heap;
use Cycle\ORM\ORMInterface;
use Cycle\ORM\Select;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Spiral\Database\Injection\Parameter;

class ApiRequestRepository extends AtaRepository implements ApiRequestRepositoryInterface
{
    protected $includeOptions = [];
    protected $enableIncludes = false;
    protected $enableFields = false;
    protected $enableFilters = false;
    protected $enableSort = false;
    protected $enablePagination = false;
    protected $enableAppends = false;

    /**
     * @var ApiRequest
     */
    protected $annotation;

    protected function newSelect()
    {
        parent::newSelect();

        $this->includeOptions = [];
        $this->enableFields = false;
        $this->enableIncludes = false;
        $this->enableFilters = false;
        $this->enableSort = false;
        $this->enablePagination = false;
        $this->enableAppends = false;
    }

    public function __construct(Select $select, ORMInterface $orm, string $role)
    {
        parent::__construct($select, $orm, $role);

        $this->annotation = resolve(ApiRequestSchema::class)->getAnnotation($this->entityClass);
    }

    /**
     * @inheritDoc
     */
    function withFields(): ApiRequestRepositoryInterface
    {
        $this->enableFields = true;
        return $this;
    }

    /**
     * @inheritDoc
     */
    function withIncludes(): ApiRequestRepositoryInterface
    {
        $this->enableIncludes = true;
        return $this;
    }

    /**
     * @inheritDoc
     */
    function withPagination(): ApiRequestRepositoryInterface
    {
        $this->enablePagination = true;
        return $this;
    }

    /**
     * @inheritDoc
     */
    function withSorts(): ApiRequestRepositoryInterface
    {
        $this->enableSort = true;
        return $this;
    }

    /**
     * @inheritDoc
     */
    function withFilters(): ApiRequestRepositoryInterface
    {
        $this->enableFilters = true;
        return $this;
    }

    /**
     * @inheritDoc
     */
    function withAppends(): ApiRequestRepositoryInterface
    {
        $this->enableAppends = true;
        return $this;
    }

    /**
     * @inheritDoc
     */
    function forRequest(): ApiRequestRepositoryInterface
    {
        return $this
            ->withFields()
            ->withFilters()
            ->withPagination()
            ->withSorts()
            ->withIncludes()
            ->withAppends();
    }

    public function findAll(array $scope = []): iterable
    {
        if ($this->fromRequest()){
            $this->initializeRequest();
        }

        if ($this->enableFilters){
            $this->processFilters();
        }
        if ($this->enablePagination){
            $this->processPagination();
        }
        if ($this->enableIncludes){
            $this->processIncludes();
        }
        // hack for new select in parent::findAll
        $enableAppends = $this->enableAppends;
        $result = parent::findAll($scope);
        if ($enableAppends){
            $this->processAppends($result);
        }

        return $result;
    }

    public function findOne(array $scope = []): ?object
    {
        // Здесь должна происходить сборка всего и вся
        return parent::findOne($scope);
    }

    protected function fromRequest():bool
    {
        return $this->enableFields ||
            $this->enableFilters ||
            $this->enablePagination ||
            $this->enableSort ||
            $this->enableIncludes;
    }

    protected function initializeRequest()
    {
        $schema = $this->createSchemaFromRequest();
        $this->orm = resolve('cycle-db')->withSchema($schema)->withHeap(new Heap());

        parent::newSelect();

        if (ExtendedRequest::fromRequest()->withoutConstrains()){
            $this->withoutConstrains();
        }
    }

    protected function createSchemaFromRequest()
    {
        $fieldMap = $this->getAvailableFields();

        $generator = new FilterColumnsGenerator();

        $fieldMap->each(function ($fields, $modelClass) use ($generator) {
            $generator->addFilter($modelClass, $fields);
        });

        return SchemaCreator::createSchema(array_merge(config('cycle.schema.generators'), [$generator]));
    }

    protected function getAvailableFields()
    {
        // go recursively via annotation and fields from request
        // we need to find all fields from whole tree

        // получаем данные из реквеста
        // Если их нет - тогда берем все поля, что есть в сущности
        // Если они есть, тогда селектом выбираем только требуемые филды

        // Так же берем все поля из related сущностей
        $fields = ExtendedRequest::fromRequest()->fields()->mapWithKeys(function (Collection $requestFields, string $key) {
            $annotation = $this->annotation;
            $className = null;

            foreach (explode(config('api_request.related_delimiter'), $key) as $relatedKey) {
                if ($annotation->getIncludes()->has($relatedKey)) {
                    $className = $annotation->getIncludes()[$relatedKey];
                    $annotation = resolve(ApiRequestSchema::class)->getAnnotation(
                        $annotation->getIncludes()[$relatedKey]
                    );
                }
            }

            $className = $className ?? $this->annotation->getModel();

            return [
                $className => $requestFields->map(function ($requestField) {
                    return $this->annotation->findField($requestField)->column;
                })
            ];
        });

        if ($fields->isNotEmpty() && $this->enableFields) {
            return $fields;
        }

        // get fields from all annotations
        return resolve(ApiRequestSchema::class)->getAllRelations($this->annotation)
            ->add($this->annotation)
            ->mapWithKeys(function (ApiRequest $elem) {
                return [
                    $elem->getModel() => $elem->getDefaultFields()->map(function ($field) {
                        return $field->column;
                    })
                ];
            });
    }

    protected function processFilters()
    {
        // get filter fields
        // filter parameters and get exact fields and exact filters
        // create where query
        $filters = new ArrayCollection();

        ExtendedRequest::fromRequest()->filters()->each(function (RequestFilter $queryFilter, $key) use ($filters) {
            $relationName = null;

            $field = $this->annotation->findField($queryFilter->field);

            if (is_null($queryFilter->operator)) {
                $queryFilter->operator = $field->default;
            }

            throw_if(!in_array($queryFilter->operator, $field->filters), CannotQueryThroughFilterException::class, $queryFilter->operator);

            // Для этих операторов необходимо оборачивать массив в этот объект
            if (in_array($queryFilter->operator, ['in', 'not_in'])) {
                $queryFilter->value = new Parameter($queryFilter->value);
            }

            if ($queryFilter->operator == 'is_null' && !$queryFilter->value){
                $queryFilter->operator = 'is_not_null';
            } elseif ($queryFilter->operator == 'is_not_null' && !$queryFilter->value){
                $queryFilter->operator = 'is_null';
            }

            if (in_array($queryFilter->operator, ['like_sensitive', 'like_insensitive'])) {
                $queryFilter->value = '%' . $queryFilter->value .'%';
            }

            if (in_array($queryFilter->operator, ['is_null', 'is_not_null'])) {
                $queryFilter->value = new Parameter(null);
            }

            if ($queryFilter->isNested) {
                if (!key_exists($queryFilter->relation, $this->includeOptions)) {
                    $this->includeOptions[$queryFilter->relation] = ['sorts' => collect(), 'where' => []];
                }

                $this->includeOptions[$queryFilter->relation]['where'] = array_merge(
                    $this->includeOptions[$queryFilter->relation]['where'], [
                        $queryFilter->field => [
                            config('api_request.operators.' . $queryFilter->operator . '.db') => $queryFilter->value
                        ]
                    ]
                );
                return;
            }

            $filters[$field->fullPath] = [
                config('api_request.operators.' . $queryFilter->operator . '.db') => $queryFilter->value
            ];
        });
        if (!$filters->isEmpty()) {
            $this->where($filters->toArray());
        }
    }

    protected function processPagination()
    {
        list($pageNumber, $perPage) = ExtendedRequest::fromRequest()->getPageInfo();

        if (!$this->annotation->isPaginatedByDefault()
            && is_null($pageNumber) && is_null($perPage)){
            return;
        }

        if ($this->enableSort){
            $this->processSortForPagination();
        }

        $pageNumber = $pageNumber ?? 1;
        $perPage = $perPage ?? $this->annotation->getPageCount();

        $this->paginate($perPage, $pageNumber);
    }

    public function processSortForPagination()
    {
        $sorts = collect();

        ExtendedRequest::fromRequest()->sorts()->each(function ($sort, $relationName) use ($sorts) {
            if (!is_string($relationName)) {
                $sorts->add($this->annotation->getSort($sort));
            }
        });

        if ($sorts->isEmpty()) {
            $sorts = $this->annotation->getDefaultSort();
        }

        $this->processSortWithCallback($sorts, function ($field, $direction) {
            if (!Str::contains($field, config('api_request.related_delimiter'))) {
                $this->select->orderBy($field, $direction);
            }
        });

        return $this;
    }

    private function processSortWithCallback(Collection $sorts, callable $callback)
    {
        $sorts->each(function ($sort) use ($callback) {
            $direction = 'ASC';

            if (Str::startsWith($sort, '-')) {
                $sort = Str::substr($sort, 1);
                $direction = 'DESC';
            }

            $callback($sort, $direction);
        });
    }

    protected function processIncludes()
    {
        if ($this->withSorts()){
            $this->processSortForIncludes();
        }

        ExtendedRequest::fromRequest()->includes()->each(function ($include) {
            if (!key_exists($include, $this->includeOptions)) {
                $this->includeOptions[$include] = ['sorts' => collect(), 'where' => []];
            }
        });

        $includes = collect($this->includeOptions);

        if ($includes->isEmpty()) {
            $includes = $this->annotation->getDefaultIncludes()->keys()->mapWithKeys(function ($elem) {
                return [$elem => ['sorts' => collect(), 'where' => []]];
            });
        }

        $includes->each(function ($relationOptions, $relationName) {
            $sorts = $relationOptions['sorts'];

            if ($sorts->isEmpty()) {
                $sorts = resolve(ApiRequestSchema::class)
                    ->getAnnotation($this->annotation->getIncludes()[$relationName])
                    ->getDefaultSort();
            }

            $this->results = $this->loadLeftJoin($relationName, [
                'load' => function (Select\QueryBuilder $qb) use ($sorts) {
                    $this->processSortWithCallback(
                        $sorts,
                        function ($field, $direction) use ($qb) {
                            $qb->orderBy($field, $direction);
                        });
                },
                'where' => $relationOptions['where']
            ]);
        });
    }

    public function processSortForIncludes()
    {
        $sorts = collect();

        ExtendedRequest::fromRequest()->sorts()->each(function ($sort, $relationName) use ($sorts) {
            if (!is_string($relationName) && !Str::contains($sort, config('api_request.related_delimiter'))) {
                $sorts->add($this->annotation->getSort($sort));
                return;
            }

            if (!is_string($relationName) && Str::contains($sort, config('api_request.related_delimiter'))) {
                list($relationName, $sort) = array_pad(explode(config('api_request.related_delimiter'), $sort), 2, null);
            }

            if (!key_exists($relationName, $this->includeOptions)) {
                $this->includeOptions[$relationName] = ['sorts' => collect(), 'where' => []];
            }
            $this->includeOptions[$relationName]['sorts']->add($this->annotation->getSort($sort));
        });

        if ($sorts->isEmpty()) {
            $sorts = $this->annotation->getDefaultSort();
        }
        $this->processSortWithCallback($sorts, function ($field, $direction) {
            if (!Str::contains($field, config('api_request.related_delimiter'))) {
                $this->select->orderBy($field, $direction);
            } else {
                list($relationName, $sort) = array_pad(explode(config('api_request.related_delimiter'), $field), 2, null);

                if (!key_exists($relationName, $this->includeOptions)) {
                    $this->includeOptions[$relationName] = ['sorts' => collect(), 'where' => []];
                }
                $this->includeOptions[$relationName]['sorts']->add($this->annotation->getSort($sort));
            }
        });
        return $this;
    }

    protected function processAppends(Collection $result)
    {
        $appends = ExtendedRequest::fromRequest()->appends();

        $diff = $appends->diff($this->annotation->getAppends());
        throw_if($diff->count() > 0, CannotFindAppendException::class, $diff);

        if ($appends->isEmpty()) {
            $appends = $this->annotation->getDefaultAppends();
        }
        return $result->each(function ($elem) use ($appends) {
            $appends->each(function ($append) use ($elem) {
                // compile functions to fields
                $elem->{$append} = $elem->{$append}();
            });
        });
    }
}
