<?php


namespace Ata\Cycle\ApiRequest\Traits;

/**
 * Trait RequestQuerying
 * @package Ata\Cycle\ApiRequest\Traits
 *
 * @method static static forRequest();
 *
 * @method static static withAppends();
 * @method static static withFilters();
 * @method static static withSorts();
 * @method static static withPagination();
 * @method static static withIncludes();
 *
 */
trait RequestQuerying
{
}
