<?php

namespace Ata\Cycle\ApiRequest\Schema;

use Ata\Cycle\ApiRequest\Annotations\ApiRequest;
use Illuminate\Support\Collection;

class ApiRequestRelationConverter
{

    /**
     * Array of visited objects; used to prevent cycling.
     *
     * @var array
     */
    protected $visited = [];

    /**
     * Determine if an object has been serialized already.
     *
     * @param mixed $value
     * @return bool
     */
    protected function wasVisited(&$value)
    {
        return in_array($value, $this->visited, true);
    }

    public function extract(ApiRequest $annotation):Collection
    {
        return $annotation->getIncludes()->map(function ($class, $relationName) {
            $annotation = resolve(ApiRequestSchema::class)->getAnnotation($class);

            if ($this->wasVisited($annotation)){
                return [];
            }
            $this->visited[] = $annotation;

            return $this->extract($annotation)->add($annotation);
        })->flatten();
    }
}
