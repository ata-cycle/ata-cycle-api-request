<?php
declare(strict_types=1);

namespace Ata\Cycle\ApiRequest\Annotations;

/**
 * @Annotation
 * @Target({"PROPERTY", "ANNOTATION"})
 * @Attributes({
 *      @Attribute("name", type="string"),
 *      @Attribute("filters", type="array<string>"),
 *      @Attribute("sort", type="bool"),
 *      @Attribute("default", type="string"),
 * })
 */
class DateRequestField extends RequestField
{
}
