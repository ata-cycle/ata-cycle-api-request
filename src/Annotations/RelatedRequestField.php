<?php


namespace Ata\Cycle\ApiRequest\Annotations;


/**
 * @Annotation
 * @Target({"PROPERTY", "ANNOTATION"})
 * @Attributes({
 *      @Attribute("name", type="string"),
 *      @Attribute("class", type="string")
 * })
 */
class RelatedRequestField extends RequestField
{
    /** @var string */
    public $class;
}
