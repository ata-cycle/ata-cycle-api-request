<?php


namespace Ata\Cycle\ApiRequest\Annotations;

use Ata\Cycle\ORM\Typecasts\Carbon;
use Ata\Cycle\ORM\Typecasts\Json;
use Doctrine\Common\Annotations\Annotation\Attribute;
use Doctrine\Common\Annotations\Annotation\Attributes;
use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target({"PROPERTY", "ANNOTATION"})
 * @Attributes({
 *      @Attribute("name", type="string", required=True),
 *      @Attribute("filters", type="array<string>"),
 *      @Attribute("sort", type="bool"),
 *      @Attribute("default", type="string"),
 *      @Attribute("column", type="string"),
 * })
 */
class RequestField
{
    /** @var array<string> */
    public $filters = [];

    /** @var bool */
    public $sort;

    /** @var string */
    public $name;

    /** @var string */
    public $default;

    /** @var string */
    public $column;

    /** @var string */
    public $fullPath;

    /**
     * @param array $values
     */
    public function __construct(array $values)
    {
        foreach ($values as $key => $value) {
            $this->$key = $value;
        }

        // enable all filters by default
        if (!array_key_exists('filters', $values)) {
            $this->filters = array_keys(config('api_request.operators'));
        }

        if (!array_key_exists('default', $values)) {
            $this->default = config('api_request.default_filters.' . get_called_class());
        }

        if (!array_key_exists('column', $values)) {
            $this->column = $this->name;
        }

        if (!array_key_exists('sort', $values)) {
            $this->sort = config('api_request.fields_sorting_by_default');
        }
    }

    public function setFullPath($fullPath)
    {
        $this->fullPath = $fullPath;
        return $this;
    }

    public static function fromSchema($name, $type)
    {
        if ($name === $type) {
            $type = 'string';
        }

        switch ($type) {
            case "bool":
            case "int":
            case "float":
                return new RequestField(['name' => $name]);
            case "string":
                return new StringRequestField(['name' => $name]);
            case "datetime":
                return new DateRequestField(['name' => $name]);
            default:
                switch ($type[0]) {
                    case Json::class:
                        return new RequestField(['name' => $name]);
                    case Carbon::class:
                        return new DateRequestField(['name' => $name]);
                }
                return new RequestField(['name' => $name]);
        }
    }
}
