<?php

declare(strict_types=1);


namespace Ata\Cycle\ApiRequest\Annotations;

use Ata\Cycle\ApiRequest\Exceptions\CannotFindFilterFieldException;
use Ata\Cycle\ApiRequest\Exceptions\CannotSortException;
use Ata\Cycle\ApiRequest\Schema\ApiRequestSchema;
use Cycle\ORM\Relation;
use Cycle\ORM\SchemaInterface;
use Doctrine\Common\Annotations\Annotation\Attribute;
use Doctrine\Common\Annotations\Annotation\Attributes;
use Doctrine\Common\Annotations\Annotation\Target;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 * @Attributes({
 *      @Attribute("model", type="string"),
 *      @Attribute("fields", type="array<Ata\Cycle\ApiRequest\Annotations\RequestField>"),
 *      @Attribute("defaultFields", type="array<string>"),
 *      @Attribute("defaultIncludes", type="array<string>"),
 *      @Attribute("defaultAppends", type="array<string>"),
 *      @Attribute("includes", type="array<string>"),
 *      @Attribute("appends", type="array<string>"),
 *      @Attribute("defaultSort", type="string"),
 *      @Attribute("paginateByDefault", type="bool"),
 *      @Attribute("perPage", type="integer"),
 * })
 */
class ApiRequest
{
    /** @var array<RequestField> */
    private $fields = [];

    /** @var array<RequestField> */
    private $defaultFields = [];

    /** @var array<string> */
    private $defaultIncludes = [];

    /** @var array<string> */
    private $includes = [];

    /** @var string */
    private $model;

    /** @var string */
    private $defaultSort;

    /** @var array<string> */
    private $appends = [];

    /** @var array<string> */
    private $defaultAppends = [];

    /** @var bool */
    private $paginateByDefault = false;

    /** @var integer */
    private $perPage;

    /**
     * @param array $values
     */
    public function __construct(array $values)
    {
        foreach ($values as $key => $value) {
            $this->$key = $value;
        }

        $this->defaultSort = $this->defaultSort ?? config('api_request.default_sort_field');
        $this->perPage = $this->perPage ?? config('api_request.pagination.per_page');
        $this->paginateByDefault = $this->paginateByDefault || config('api_request.pagination.enabled_by_default');
    }

    /**
     * @return Collection
     */
    public function getFields(): Collection
    {
        return collect($this->fields);
    }

    /**
     * @return Collection
     */
    public function getIncludes(): Collection
    {
        return collect($this->includes);
    }

    /**
     * @return Collection
     */
    public function getDefaultFields(): Collection
    {
        $collection = $this->getFields()->filter(function ($elem) {
            return in_array($elem->name, $this->defaultFields);
        });
        return $collection;
    }

    /**
     * @return Collection
     */
    public function getDefaultIncludes(): Collection
    {
        return $this->getIncludes()->filter(function($className, $key){
            return in_array($key, $this->defaultIncludes);
        });
    }
    /**
     * @return Collection
     */
    public function getDefaultAppends(): Collection
    {
        return $this->getAppends()->filter(function($append){
            return in_array($append, $this->defaultAppends);
        });
    }

    /**
     * @return Collection
     */
    public function getAppends(): Collection
    {
        return collect($this->appends);
    }

    /**
     * @param string $fieldNameDefinition
     * @param bool $findForSort
     * @return RequestField
     * @throws \Throwable
     */
    public function findField(string $fieldNameDefinition, bool $findForSort = false): RequestField
    {
        $annotation = $this;
        $field = null;
        $fullPath = null;

        foreach (explode(config('api_request.related_delimiter'), $fieldNameDefinition) as $fieldName) {
            $field = $annotation->getFields()
                ->first(function (RequestField $f) use ($fieldName) {
                    return $f->name == $fieldName || $f->column == $fieldName;
                });

            if (is_null($field)) {
                $annotation = resolve(ApiRequestSchema::class)->getAnnotation(
                    $annotation->getIncludes()[$fieldName]
                );

                $fullPath = $fullPath ? $fullPath . '.' . $fieldName : $fieldName;
            } else {
                $fullPath = $fullPath ? $fullPath . '.' . $field->column : $field->column;
            }

        };

        throw_if(is_null($field), CannotFindFilterFieldException::class, $fieldNameDefinition);
        throw_if($findForSort && !$field->sort, CannotSortException::class, $fieldNameDefinition);

        return $field->setFullPath($fullPath);
    }

    public function forModel($className)
    {
        $this->model = $className;

        $schema = resolve('cycle-db')->getSchema();

        $this->syncFields($schema);
        $this->syncIncludes($schema);

        return $this;
    }

    public function syncFields(SchemaInterface $schema){
        $fields = $schema->define($this->model, SchemaInterface::COLUMNS);
        $fields += $schema->define($this->model, SchemaInterface::TYPECAST);
        $existingFields = $this->getFields();

        foreach ($fields as $key => $value) {
            $field = RequestField::fromSchema($key, $value);

            if ($existingFields->filter(
                    function ($elem) use ($field) {
                        return $elem->name === $field->name;
                    })->count() == 0) {
                $this->fields[] = $field;
            }
        }
    }

    public function syncIncludes(SchemaInterface $schema)
    {
        $relations = $schema->define($this->model, SchemaInterface::RELATIONS);

        foreach($relations as $name => $properties){
            if (array_key_exists($name, $this->includes)){
                continue;
            }

            if ($properties[Relation::TYPE] === Relation::BELONGS_TO_MORPHED){
                continue;
            }

            $this->includes[$name] = $schema->define($properties[Relation::TARGET], SchemaInterface::ENTITY);
        }
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getSort(string $fieldName)
    {
        if (Str::startsWith($fieldName, '-')) {

            return '-' . $this->findField(Str::substr($fieldName, 1), true)->fullPath;
        }

        return $this->findField($fieldName, true)->fullPath;
    }

    public function getDefaultSort()
    {
        // I love php for callback syntax
        return collect(explode(',', $this->defaultSort))->map([$this, 'getSort']);
    }

    public function isPaginatedByDefault()
    {
        return $this->paginateByDefault;
    }

    public function getPageCount()
    {
        return $this->perPage;
    }
}
